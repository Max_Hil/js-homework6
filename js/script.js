function createNewUser() {
  let user = {};
    Object.defineProperties(user, {
      'firstName': {
        value: prompt("Enter your first name", ""),
        writable: false,
        enumerable: true,
        configurable: true
      },
      'lastName': {
        value: prompt("Enter your last name", ""),
        writable: false,
        enumerable: true,
        configurable: true
      },
      'getLogin': {
        get: function() {
          return user.firstName[0].toLowerCase() + user.lastName.toLowerCase()
        }
      },
      'newFirstName': {
        set: function(newValue) {
          Object.defineProperty(this, 'firstName', {value: newValue})
        }
      },
      'newLastName': {
        set: function(newValue) {
          Object.defineProperty(this, 'lastName', {value: newValue})
        }
      }
    });
    return user;
}
const user = createNewUser();
console.log(user);
console.log(user.getLogin);
